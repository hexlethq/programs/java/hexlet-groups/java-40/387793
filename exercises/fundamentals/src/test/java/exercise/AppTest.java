package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;

class AppTest {
    @Test
    void testNumbers() throws Exception {
        String result = tapSystemOut(() -> {
            App.numbers();
            System.out.println(8 / 2 + 100 % 3);
        });
        assertThat(result.trim()).isEqualTo("5");
    }

    @Test
    void testStrings() throws Exception {
        String result = tapSystemOut(() -> {
            App.strings();
            System.out.println("Java works on JVM");
        });
        assertThat(result.trim()).isEqualTo("Java works on JVM");
    }

    @Test
    void testConverting() throws Exception {
        String result = tapSystemOut(() -> {
            App.converting();
            System.out.println(300 + spartans);
        });
        assertThat(result.trim()).isEqualTo("300 spartans");
    }
}
